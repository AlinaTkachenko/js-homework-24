//зробили поле
const body = document.querySelector('body');
const table = document.createElement('table');
const btnNewPlay = document.createElement('button');

body.prepend(table);
let counter = 1;
for (let i = 0; i < 8; i++) {
    const tr = document.createElement('tr');
    table.append(tr);
    for (let i = 0; i < 8; i++) {
        const td = document.createElement('td');
        td.setAttribute('id', `${counter}`);
        td.classList.add('invisible');
        counter++;
        tr.append(td);
    };
};

const trs = Array.from(document.querySelectorAll('tr'));

//генерація випадкових чисел
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
};

//зробили міни
let mines = [];

function generateNumber() {
    const mine = getRandomIntInclusive(1, 64);
    if (!mines.includes(mine)) {
        mines.push(mine);
    } else {
        generateNumber();
    };
};

function generateMines() {
    for (let i = 0; i < 10; i++) {
        generateNumber();
    }

    mines.forEach(function (element) {
        const square = document.getElementById(`${element}`);
        square.innerHTML = `<img class="imgBomba" src="./img/Bomb-PNG-Image.png" width="30px" height="30px">`
        square.classList.add('invisible');
        square.classList.add('bomba');
    });
};
generateMines();

//виводимо числа
function setNumber() {

    let arrtdLeft = [];
    let arrtdRight = [];

    mines.forEach(function (mine) {

        const arrNumberMinus = [7, 8];
        const arrNumberPlus = [1, 8, 9];
        const arrNumber = [1, 7, 8, 9];

        if (mine === 1 || mine === 9 || mine === 17 || mine === 25 || mine === 33 || mine === 41 || mine === 49 || mine === 57) {
            arrNumberMinus.forEach(function (elem) {
                let numberLeft = mine - elem;

                if (numberLeft > 0) {
                    const tdLeft = document.getElementById(`${numberLeft}`);

                    if (tdLeft.innerText == '' && !tdLeft.hasChildNodes()) {
                        tdLeft.innerText = '1';
                    } else {
                        arrtdLeft.push(tdLeft);
                    };
                };
            });

            arrNumberPlus.forEach(function (elem) {
                let numberRight = mine + elem;

                if (numberRight > 0 && numberRight < 65) {
                    const tdRight = document.getElementById(`${numberRight}`);
                    if (tdRight.innerText == '' && !tdRight.hasChildNodes()) {
                        tdRight.innerText = '1';
                    } else {
                        arrtdRight.push(tdRight);
                    };
                };
            });

        };

        if (mine === 8 || mine === 16 || mine === 24 || mine === 32 || mine === 40 || mine === 48 || mine === 56 || mine === 64) {
            arrNumberPlus.forEach(function (elem) {
                let numberLeft = mine - elem;

                if (numberLeft > 0) {
                    const tdLeft = document.getElementById(`${numberLeft}`);

                    if (tdLeft.innerText == '' && !tdLeft.hasChildNodes()) {
                        tdLeft.innerText = '1';
                    } else {
                        arrtdLeft.push(tdLeft);
                    };
                };
            });

            arrNumberMinus.forEach(function (elem) {
                let numberRight = mine + elem;

                if (numberRight > 0 && numberRight < 65) {
                    const tdRight = document.getElementById(`${numberRight}`);
                    if (tdRight.innerText == '' && !tdRight.hasChildNodes()) {
                        tdRight.innerText = '1';
                    } else {
                        arrtdRight.push(tdRight);
                    };
                };
            });

        };

        if (mine != 1 && mine != 9 && mine != 17 && mine != 25 && mine != 33 && mine != 41 && mine != 49 && mine != 57 && mine != 8 && mine != 16 && mine != 24 && mine != 32 && mine != 40 && mine != 48 && mine != 56 && mine != 64) {
            arrNumber.forEach(function (elem) {
                let numberLeft = mine - elem;

                if (numberLeft > 0) {
                    const tdLeft = document.getElementById(`${numberLeft}`);

                    if (tdLeft.innerText == '' && !tdLeft.hasChildNodes()) {
                        tdLeft.innerText = '1';
                    } else {
                        arrtdLeft.push(tdLeft);
                    };
                };
                let numberRight = mine + elem;

                if (numberRight > 0 && numberRight < 65) {
                    const tdRight = document.getElementById(`${numberRight}`);

                    if (tdRight.innerText == '' && !tdRight.hasChildNodes()) {
                        tdRight.innerText = '1';
                    } else {
                        arrtdRight.push(tdRight);
                    };
                };
            });

        };

        arrtdLeft = arrtdLeft.filter(item => !item.classList.contains('bomba'));
        arrtdRight = arrtdRight.filter(item => !item.classList.contains('bomba'));
        arrNew = [...arrtdLeft, ...arrtdRight];

        arrNew.forEach(function (element) {
            const arr = arrNew.filter(item => item === element);
            const elem = document.getElementById(`${element.id}`);
            elem.innerText = `${arr.length + 1}`;
        });
    });

};

setNumber();

//робимо координати клітинок для подальшого використання у виводі пустих клітинок
let coordinates = [];

function setСoordinates() {
    let countY = 0;
    let countX = 0;
    let countIndex = 0;

    trs.forEach(function (tr) {
        coordinates.push([]);
        let tds = Array.from(tr.childNodes); //клетки

        tds.forEach(function (td) {
            coordinates[countIndex].push({
                id: td.id,
                X: countX,
                Y: countY,
                figure: td.innerText === "" ? false : true,
                bomba: td.classList.contains('bomba')
            });
            countX++;

        });
        countX = 0;
        countY++;
        countIndex++;
    });
    console.log(coordinates)
}

setСoordinates();

//вивід пустих клітинок

function checkCoordAvailability(x, y, prevX, prevY) {
    // перевірили що не випали за поле
    if (x < 0 || y < 0 || x > 7 || y > 7) {
        return false;
    };

    // перевірили що прийшли не з цієї точки
    if (x === prevX && y === prevY) {
        return false;
    };

    let square = coordinates[y][x];

    // перевірили чи порожня клітинка
    if (square.figure || square.bomba || square.color) {
        return false;
    }
    return true;
};

//зафарбували і рекурсія
function checkAndFillCell(x, y, prevX, prevY) {
    let square = coordinates[y][x];

    document.getElementById(square.id).classList.remove('invisible');
    document.getElementById(square.id).classList.add('green');
    square.color = true;

    prevX = x;
    prevY = y;

    getEmptySquare(x, y - 1, prevX, prevY);
    getEmptySquare(x, y + 1, prevX, prevY);
    getEmptySquare(x - 1, y, prevX, prevY);
    getEmptySquare(x + 1, y, prevX, prevY);
}

//виводимо цифри навколо пустих клітинок
function getNumber(x, y) {
    const arr = [
        [x - 1, y],
        [x + 1, y],
        [x, y - 1],
        [x, y + 1],
        [x - 1, y - 1],
        [x + 1, y + 1],
        [x - 1, y + 1],
        [x + 1, y - 1]
    ];

    arr.forEach(function (item) {
        const _x = item[0];
        const _y = item[1];
        let square = coordinates[_y] && coordinates[_y][_x];

        if (square) {
            document.getElementById(square.id).classList.remove('invisible');
            document.getElementById(square.id).classList.add('visible');
        };
    });
};

function getEmptySquare(x, y, prevX, prevY) {

    if (checkCoordAvailability(x, y, prevX, prevY)) {
        getNumber(x, y);
        checkAndFillCell(x, y, prevX, prevY);
    }
}

//клік на кнопку розпочати нову гру
function startNewGame() {
    btnNewPlay.remove();
    const tds = document.querySelectorAll('td');
    tds.forEach(function (td) {
        td.classList.remove('visible');
        td.classList.remove('green');
        td.classList.remove('bomba');
        td.classList.add('invisible');
        td.innerText = '';
        mines = [];
        if (td.hasChildNodes()) {
            td.removeChild();
        }
    });

    table.style.pointerEvents = 'auto';
    generateMines();
    setNumber();
}

//клік по полю
table.addEventListener('click', function (event) {
    btnNewPlay.setAttribute('type', 'button');
    btnNewPlay.innerText = 'Почати спочатку';
    btnNewPlay.classList.add('btn');
    body.prepend(btnNewPlay);
    btnNewPlay.addEventListener('click', startNewGame)

    if (event.target.tagName === 'IMG') {
        table.style.pointerEvents = 'none';
        const bombs = document.querySelectorAll('.bomba');
        bombs.forEach(function (element) {
            element.classList.remove('invisible');
            element.classList.add('visible');
        });
    };
    if (event.target.tagName === 'TD') {
        event.target.classList.remove('invisible');
        event.target.classList.add('visible');
        if (event.target.innerText === '') {
            const clickId = event.target.id;
            let x;
            let y;
            coordinates.forEach(function (element) {
                element.forEach(function (obj) {
                    if (obj.id === clickId) {
                        x = obj.X;
                        y = obj.Y;
                    }
                })
            })
            getEmptySquare(x, y);
        }
    };
})

//табло
let flag = 0;
const tablo = document.createElement('div');
tablo.classList.add('tablo');
tablo.innerText = `Міни:10/Прапорці${flag}`;
body.prepend(tablo);

//прапорці
table.oncontextmenu = function (event) {
    event.preventDefault();
    
    if (event.target.classList.contains('imgBomba')) {
        const td = event.target.closest('TD');
        td.classList.remove('invisible');
        td.classList.add('visible');
        event.target.insertAdjacentHTML('afterend', '<img class="imgFlag" src="./img/flag-of-ukraine-ukraine-national-flag-icon_118339-1983.avif" width="30px" height="30px">');
        event.target.remove();
        flag++;
        tablo.innerText = `Міни:10/Прапорці${flag}`;
    };
    if (event.target.classList.contains('imgFlag')) {
        const td = event.target.closest('TD');
        td.classList.remove('visible');
        td.classList.add('invisible');
        event.target.insertAdjacentHTML('afterend', '<img class="imgBomba" src="./img/Bomb-PNG-Image.png" width="30px" height="30px">');
        event.target.remove();
        flag--;
        tablo.innerText = `Міни:10/Прапорці${flag}`;
    }
};